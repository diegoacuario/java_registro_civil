
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author CEL
 */
public class FuncionesGet {

    /**
     *
     * @param url url para realizar la consulta
     * @return un String con el texto contenido en la url
     */
    public String obtieneDeUrlPorGet(String url) {
        String texto = "", linea;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
            while ((linea = in.readLine()) != null) {
                texto += linea + "\n";
            }
        } catch (UnknownHostException ex) {
            return "null desc " + ex;
        } catch (NoRouteToHostException ex) {
            return "null nrou " + ex;
        } catch (ConnectException ex) {
            return "null cone " + ex;
        } catch (SocketException ex) {
            return "null sock " + ex;
        } catch (SocketTimeoutException ex) {
            return "null stim " + ex;
        } catch (IOException ex) {
            return "null io   " + ex;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            return "null      " + ex;
        }
        return texto.substring(0, texto.length() - 1);
    }

}

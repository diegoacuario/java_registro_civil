
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author CEL
 */
public class Funciones {

    Gson gson = new Gson();
    FuncionesGet fg = new FuncionesGet();

    public String reempazarTildesEnies(String json) {
        json = json.replaceAll("&Aacute;", "Á");
        json = json.replaceAll("&Eacute;", "É");
        json = json.replaceAll("&Iacute;", "Í");
        json = json.replaceAll("&Oacute;", "Ó");
        json = json.replaceAll("&Uacute;", "Ú");
        json = json.replaceAll("&Ntilde;", "Ñ");
        json = json.replaceAll("&aacute;", "á");
        json = json.replaceAll("&eacute;", "é");
        json = json.replaceAll("&iacute;", "í");
        json = json.replaceAll("&oacute;", "ó");
        json = json.replaceAll("&uacute;", "ú");
        json = json.replaceAll("&ntilde;", "ñ");
        return json;
    }

    public Date fechaHoy() {
        return new Date();
    }

    public String corrigeFrasesCameEspacios(String e) {
        String text = "";
        for (int i = 0; i < e.length(); i++) {
            if (i == 0) {
                text += e.substring(i, i + 1).toUpperCase();
            } else if (e.substring(i - 1, i).equals(" ")) {
                text += e.substring(i, i + 1).toUpperCase();
            } else {
                text += e.substring(i, i + 1).toLowerCase();
            }
        }
        return text;
    }

    public String parsearFechaHora(Date d) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
    }

    public String parsearFecha(Date d) {
        return new SimpleDateFormat("yyyy-MM-dd").format(d);
    }

    public Personas buscarPersonasPorCedulaRegCivil(String cedula) {
        String json = fg.obtieneDeUrlPorGet("http://diego.ec/servicios/reg_civil/?ci=" + cedula), error = "";
        Date fechaNacimiento = fechaHoy(), hoy = fechaHoy();
        String apellidoNombre = "-",
                datNomApe[], nombre = "-", apellido = "-", nacionalidad = "-",
                parroquia = "-", direccion = "-", numeroCasa = "-", genero = "-";
        Personas p = new Personas(cedula, nombre, apellido, apellidoNombre, fechaNacimiento, genero, nacionalidad, parroquia, direccion, numeroCasa, hoy);
        if (json.contains("null") || json.contains("Error al consultar los datos")) {
            error = json;
            System.out.println("Cédula: \"" + cedula + "\" " + error + parsearFechaHora(hoy));
        } else {
            json = reempazarTildesEnies(json);
            json = json.substring(1, json.length() - 1);
            Properties properties = gson.fromJson(json, Properties.class);
            error = properties.getProperty("error");
            if (error.length() == 0) {
                apellidoNombre = properties.getProperty("name");
                datNomApe = apellidoNombre.split(" ");
                switch (datNomApe.length) {
                    case 4:
                        nombre = datNomApe[2] + " " + datNomApe[3];
                        apellido = datNomApe[0] + " " + datNomApe[1];
                        break;
                    case 2:
                        nombre = datNomApe[1];
                        apellido = datNomApe[0];
                        break;
                    case 3:
                        apellido = datNomApe[0];
                        break;
                    case 5:
                        nombre = datNomApe[2] + " " + datNomApe[3] + " " + datNomApe[4];
                        apellido = datNomApe[0] + " " + datNomApe[1];
                        break;
                    case 6:
                        nombre = datNomApe[2] + " " + datNomApe[3] + " " + datNomApe[4] + " " + datNomApe[5];
                        apellido = datNomApe[0] + " " + datNomApe[1];
                        break;
                    case 7:
                        nombre = datNomApe[2] + " " + datNomApe[3] + " " + datNomApe[4]
                                + " " + datNomApe[5] + " " + datNomApe[6];
                        apellido = datNomApe[0] + " " + datNomApe[1];
                        if ((datNomApe[1] + datNomApe[2]).contains("DE LA")) {
                            nombre = datNomApe[4]
                                    + " " + datNomApe[5] + " " + datNomApe[6];
                            apellido = datNomApe[0] + " " + datNomApe[1] + " " + datNomApe[2] + " " + datNomApe[3];
                        } else if ((datNomApe[1] + datNomApe[2]).contains("DE")) {
                            nombre = datNomApe[3] + " " + datNomApe[4] + " " + datNomApe[5] + " " + datNomApe[6];
                            apellido = datNomApe[0] + " " + datNomApe[1] + " " + datNomApe[2];
                        }
                        break;
                    default:
                        break;
                }
                String fechaNac = properties.getProperty("dob");
                try {
                    fechaNacimiento = new SimpleDateFormat("dd/MM/yyyy").parse(fechaNac.replaceAll("\"", ""));
                } catch (ParseException ex) {
                    System.out.println("Fecha de nacimiento erronea: " + fechaNac);
                }
                genero = properties.getProperty("genre");
                nacionalidad = properties.getProperty("nationality");
                parroquia = properties.getProperty("residence");
                direccion = properties.getProperty("streets");
                numeroCasa = properties.getProperty("homenumber");
                nombre = corrigeFrasesCameEspacios(nombre);
                apellido = corrigeFrasesCameEspacios(apellido);
                apellidoNombre = corrigeFrasesCameEspacios(apellidoNombre);
                p = new Personas(cedula, nombre, apellido, apellidoNombre, fechaNacimiento, genero, nacionalidad, parroquia, direccion, numeroCasa, hoy);
            } else {
                System.out.println("Cédula: \"" + cedula + "\" " + error + parsearFechaHora(hoy));
            }
        }
        return p;
    }

    public void presentaLista(ArrayList<String> data) {
        for (int i = 0; i < data.size(); i++) {
            System.out.println(data.get(i));
        }
    }

    public String[] convertirListaAArreglo(ArrayList<String> data) {
        String[] x = new String[data.size()];
        for (int i = 0; i < data.size(); i++) {
            x[i] = data.get(i);
        }
        return x;
    }

}

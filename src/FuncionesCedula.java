

/**
 *
 * @author Ing. Diego Romero Armijos
 */
public class FuncionesCedula {

    public String completaCorrigeCedula(String cedula) {
        if (!cedula.matches("[0-9]*")) {            
            return null;
        }
        if (!provinciaValida(Integer.parseInt(cedula.substring(0, 2))) || Integer.parseInt(cedula.substring(2, 3)) >= 6 || cedula.length() > 10 || cedula.length() < 9) {
            System.out.println("Provincia inválida: "+cedula);
            return null;
        }
        String cheksum;
        cedula = cedula.substring(0, 9);
        int suma = 0, cadaDigito;
        for (int i = 0; i < cedula.length(); i++) {
            cadaDigito = Integer.parseInt(cedula.substring(i, i + 1)) * (2 - i % 2);
            suma += ((cadaDigito % 10) + (cadaDigito / 10));
        }
        cheksum = ((suma % 10) != 0) ? (10 - (suma % 10)) + "" : "0";
        return cedula + cheksum;
    }

    public String corrigeCedulaNumerica(String cedula) {
        if (!provinciaValida(Integer.parseInt(cedula.substring(0, 2))) || Integer.parseInt(cedula.substring(2, 3)) >= 6 || cedula.length() > 10 || cedula.length() < 9) {
            return null;
        }
        int suma = 0, cheksum;
        for (int i = 0; i < cedula.length(); i++) {
            suma += Integer.parseInt(cedula.substring(i, i + 1)) * (2 - i % 2) % 10 + Integer.parseInt(cedula.substring(i, i + 1)) * (2 - i % 2) / 10;
        }
        cheksum = suma % 10 != 0 ? 10 - suma % 10 : 0;
        return cedula + cheksum;
    }

    public boolean cedulaValidaEcuador(String cedula) {
        try {
            if (cedula.length() != 10 || !cedula.matches("[0-9]*")) {
                return false;
            }
        } catch (NullPointerException e) {
            return false;
        }
        return corrigeCedulaNumerica(cedula.substring(0, 9)).equals(cedula) && provinciaValida(Integer.parseInt(cedula.substring(0, 2))) && Integer.parseInt(cedula.substring(2, 3)) < 6;
    }

    public boolean provinciaValida(int pro) {
        return pro >= 1 && pro <= 24;
    }

    public String generaCedulaAleatoria(String pro, int terDig) {
        String ced = pro + terDig;
        int par = (int) (Math.random() * 1000000);
        String parString = par + "";
        if (par < 100000) {
            parString = "0" + parString;
            if (par < 10000) {
                parString = "0" + parString;
                if (par < 1000) {
                    parString = "0" + parString;
                    if (par < 1000) {
                        parString = "0" + parString;
                        if (par < 10) {
                            parString = "0" + parString;
                        }
                    }
                }
            }
        }
        ced += parString;
        return completaCorrigeCedula(ced);
    }

}

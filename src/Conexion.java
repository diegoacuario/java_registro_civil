
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author CEL
 */
public class Conexion {

    public Connection conectarConMysql() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //return DriverManager.getConnection("jdbc:mysql://localhost/celdb", "root", "");
            return DriverManager.getConnection("jdbc:mysql://diego.ec/diegoacu_cel", "diegoacu_cel", "loja1234*");
            //return DriverManager.getConnection("jdbc:mysql://192.168.137.1/registrocivildb", "root", "");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    public ArrayList<String> obtieneListaTablas(Connection con) {
        try {
            ArrayList<String> data = new ArrayList();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SHOW TABLES");
            while (rs.next()) {
                data.add(rs.getString(1));
            }
            rs.close();
            st.close();
            con.close();
            return data;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public ArrayList<String> obtieneListaPersonas(Connection con) {
        try {
            ArrayList<String> data = new ArrayList();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT CONCAT(nombres, ' ', apellidos), cedula FROM personas");
            while (rs.next()) {
                data.add(rs.getString(1) + "\t" + rs.getString(2));
            }
            rs.close();
            st.close();
            con.close();
            return data;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public ArrayList<Personas> obtieneDatosPersonas(Connection con) {
        try {
            ArrayList<Personas> data = new ArrayList();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM personas");
            while (rs.next()) {
                Personas p = new Personas(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDate(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getTimestamp(12),
                        rs.getTimestamp(13)
                );
                data.add(p);
            }
            rs.close();
            st.close();
            con.close();
            return data;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public Personas buscaPersonaPorCedula(Connection con, String cedula) {
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM personas WHERE cedula='" + cedula + "'");
            rs.next();
            Personas p = new Personas(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getDate(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8),
                    rs.getString(9),
                    rs.getString(10),
                    rs.getString(11),
                    rs.getTimestamp(12),
                    rs.getTimestamp(13)
            );
            rs.close();
            st.close();
            con.close();
            return p;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public boolean insercionActualizacionEliminacionBase(Connection con, String sentenciaSql) {
        try {
            Statement stmt = con.createStatement();
            stmt.execute(sentenciaSql);
            stmt.close();
            con.close();
            return true;
        } catch (SQLException ex) {

            System.out.println(ex);
            return false;
        }
    }
}
